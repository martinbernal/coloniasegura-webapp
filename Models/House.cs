using System;
using System.ComponentModel.DataAnnotations;

namespace ColoniaSegura.Models
{
    public class House
    {
        [Key]
        public Guid Id { get; set; }
        
        public string Calle { get; set; }
        
        public string Numero { get; set; }
    }
}