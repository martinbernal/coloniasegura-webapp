using System;
using System.ComponentModel.DataAnnotations;
using Microsoft.AspNetCore.Mvc;

namespace ColoniaSegura.Models
{
    public class User
    {
        [Key]
        public Guid id { get; set; }
       
        [DataType(DataType.EmailAddress)]
        [EmailAddress]
        [Required]
        [Remote("EmailInUse", "UsersController", AdditionalFields = "id", 
            ErrorMessage = "Username already exists")] 
        public string username { set; get; }
        
        [Required]
        public string password { set; get; }
        
        [Required]
        public string name { set; get; }
        
        [Required]
        public string lastname { set; get; }
        
        [Required]
        public string group_id { set; get; }
        
        [Required]
        public bool status { set; get; }

        public User()
        {
            
        }
    }
}