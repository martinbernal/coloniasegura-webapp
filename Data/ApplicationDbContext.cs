using ColoniaSegura.Models;
using Microsoft.EntityFrameworkCore;

namespace ColoniaSegura.Data
{
    public class ApplicationDbContext : DbContext
    {

        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options) : base(options)
        {
            
        }
        
        public DbSet<User> Users { get; set; }
        public DbSet<House> Houses { get; set; }
        
    }
}