import {Inject, Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {User} from "../models/user";
import {Observable} from "rxjs";

@Injectable({
  providedIn: 'root'
})
export class UserService {
  public apiRoot: string;

  constructor(private http: HttpClient, @Inject('BASE_URL') baseUrl: string) {
    this.apiRoot=baseUrl;
  }

  getDataById(id): Observable<any> {
    return this.http.get(this.apiRoot + 'users/' + id);
  }

  saveData(user: User): Observable<any> {
    return this.http.post(this.apiRoot + 'users/', user);
  }
  updateData(user: User): Observable<any> {
    return this.http.put(this.apiRoot + 'users/' + user.id, user);
  }
  deleteData(id): Observable<any> {
    return this.http.delete(this.apiRoot + 'users/' + id);
  }

  login(user: User): Observable<any> {
    return this.http.post(this.apiRoot + 'users/login', {username: user.username, password: user.password});
  }

}
