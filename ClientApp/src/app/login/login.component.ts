import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormControl, FormGroup, Validators} from "@angular/forms";
import {UserService} from "../services/user.service";
import {User} from "../models/user";

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  public loginForm: FormGroup;

  constructor(
    private fb: FormBuilder,
    private userService: UserService
    ) { }

  ngOnInit() {
    this.loginForm = this.fb.group({
      username: new FormControl('', [Validators.required, Validators.email]),
      password: new FormControl('', [Validators.required]),
      keep_sesion: new FormControl()
    });

  }

  onFormSubmit() {
    console.log("Post data", {username: this.loginForm.get('username').value, password: this.loginForm.get('password').value, keep_sesion: this.loginForm.get('keep_sesion').value})
    this.userService.login({username: this.loginForm.get('username').value, password: this.loginForm.get('password').value}).subscribe(
      resp=>{
        console.log(resp);
      },
      error => {
        console.log(error);
      }
    );
  }
}
