using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using ColoniaSegura.Models;

namespace ColoniaSegura.Services
{
    public interface IHouseService
    {
        Task<List<House>> GetHousesAsync();
        Task<House> GetHouseByIdAsync(Guid houseId);

        Task<bool> CreateHouseAsync(House house);
        
        Task<bool> UpdateHouseAsync(House houseToUpdate);

        Task<bool> DeleteHouseAsync(Guid houseId);
    }
}