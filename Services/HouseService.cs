using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ColoniaSegura.Data;
using ColoniaSegura.Models;
using Microsoft.EntityFrameworkCore;

namespace ColoniaSegura.Services
{
    public class HouseService : IHouseService
    {

        private readonly ApplicationDbContext _db;

        public HouseService(ApplicationDbContext db)
        {
            _db = db;
        }


        public async Task<List<House>>  GetHousesAsync()
        {
            return await _db.Houses.ToListAsync();
        }

        public async Task<House> GetHouseByIdAsync(Guid houseId)
        {
            return await _db.Houses.SingleOrDefaultAsync(x=> x.Id == houseId);
        }

        public async Task<bool> CreateHouseAsync(House house)
        {
            await _db.Houses.AddAsync(house);
            var created = await _db.SaveChangesAsync();
            return created>0;
        }

        public async Task<bool> UpdateHouseAsync(House houseToUpdate)
        {
            _db.Houses.Update(houseToUpdate);
            var updated = await _db.SaveChangesAsync();
            return updated>0;
        }

        public async Task<bool> DeleteHouseAsync(Guid houseId)
        {
            var house = await GetHouseByIdAsync(houseId);
            
            _db.Houses.Remove(house);
            var deleted = await _db.SaveChangesAsync();
            return deleted>0;
        }
    }
}