namespace ColoniaSegura.Contracts.House
{
    public class UpdateHouseRequest
    {
        public string Calle { set; get; }
        public string Numero { set; get; }
    }
}