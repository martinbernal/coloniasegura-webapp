namespace ColoniaSegura.Contracts.House
{
    public class CreateHouseRequest
    {
        public string Calle { set; get; }
        public string Numero { set; get; }
    }
}