using System;
using System.Linq;
using System.Threading.Tasks;
using ColoniaSegura.Data;
using ColoniaSegura.Models;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json.Linq;

namespace ColoniaSegura.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    
    public class UsersController : ControllerBase
    {

        private readonly ApplicationDbContext _db;

        public UsersController(ApplicationDbContext db)
        {
            _db = db;
        }
        
        [HttpGet]
        // GET
        public IActionResult Index()
        {
            return Ok(_db.Users.ToList());
        }

        [HttpPost]
        public async Task<IActionResult> AddUser([FromBody] User objUser)
        {
            if (!ModelState.IsValid)
            {
                return new JsonResult("Error while creating user.");
            }

            _db.Users.Add(objUser);
            await _db.SaveChangesAsync();
            
            return new JsonResult("User created Successfully");
        }
        
        
        [HttpPost]
        [Route("login")]
        public IActionResult Login(LoginRequest login)
        {
           var user=_db.Users.FirstOrDefault(x=>x.username==login.Username && x.password==login.Password);
           if (user == null)
           {
               return NotFound();
           }
           else
           {
               return Ok(user);
           }
        }

    }
}