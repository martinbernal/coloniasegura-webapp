using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ColoniaSegura.Contracts.House;
using ColoniaSegura.Models;
using ColoniaSegura.Services;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace ColoniaSegura.Controllers
{
    
    [ApiController]
    [Route("api/houses")]

    public class HousesController : Controller
    {

        private readonly IHouseService _houseService;

        public HousesController(IHouseService postService)
        {
            _houseService = postService;
        }


        [HttpGet]
        // GET
        public async Task<IActionResult> GetAll()
        {
            return Ok(await _houseService.GetHousesAsync());
        }


        [HttpGet]
        [Route("{houseId}")]
        public async Task<IActionResult> Get([FromRoute] Guid houseId)
        {
            var house =await _houseService.GetHouseByIdAsync(houseId);
            if (house == null) return NotFound();
            return Ok(house);
        }

        [HttpPost]
        public async Task<IActionResult> Create([FromBody] CreateHouseRequest createHouseRequest)
        {
            var house = new House { Calle = createHouseRequest.Calle, Numero = createHouseRequest.Calle};
            
            await _houseService.CreateHouseAsync(house);

            var baseUrl = $"{HttpContext.Request.Scheme}://{HttpContext.Request.Host.ToUriComponent()}";
            var locationUrl = baseUrl + "/api/houses/" + house.Id;
            return Created(locationUrl, house);
        }

        [HttpPut]
        [Route("{houseId}")]
        public async Task<IActionResult> Update([FromRoute] Guid houseId, [FromBody] UpdateHouseRequest request)
        {
            var house = new House { Id = houseId, Calle = request.Calle};

            var updated = await _houseService.UpdateHouseAsync(house);

            if (updated) return Ok(house);
            return NotFound();
        }


        [HttpDelete]
        [Route("{houseId}")]
        public async Task<IActionResult> Delete([FromRoute] Guid houseId)
        {
            var delete = await _houseService.DeleteHouseAsync(houseId);

            if (delete) return NoContent();
            return NotFound();
        }
    }
}