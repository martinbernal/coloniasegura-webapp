namespace ColoniaSegura.Options
{
    public class SwaggerOptions
    {
        public string JsonRoute { set; get; }
        public string Description { set; get; }
        public string UiEndpoint { set; get; }
    }
}